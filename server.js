'use strict';

const express = require('express');
var mongoose = require('mongoose');
const http = require('http');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');

const routes = require('./router');
const app = express();
const router = express.Router();

// connect to MongoDB datastore
const url = process.env.MONGODB_URI || 'mongodb://localhost:27017/firstnodeapi';
const port = process.env.PORT || 4200;
try {
  mongoose.connect(url);
  const db = mongoose.connection;
  db.on('error', console.error.bind(console, 'Database connection error:'));
  db.once('open', function() {
    console.log('Connect database successfully!');
  });
} catch (e) {
  console.log(e);
}

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

/** set up routes {API Endpoints} */
routes(router);
app.use('/api/v1', router);

// use for load static files
app.use(express.static(path.join(__dirname, 'build')));

// rewrite virtual urls to frontend app to enable refreshing of internal pages
app.get('*', function(req, res, next) {
  res.sendFile(path.resolve(path.join(__dirname, 'build/index.html')));
});

const server = http.createServer(app);
server.listen(port, () => console.log(`Running on localhost:${port}`));