const productController = require('../controllers/product.controller');

module.exports = router => {
  router.route('/products').get(productController.getAll);

  router.route('/products').post(productController.create);

  router.route('/products/:id').get(productController.getById);

  router.route('/products?code=').get(productController.getByCode);

  router.route('/products/providerId=').get(productController.getByProviderId);

  router.route('/products/:id').delete(productController.deleteProduct);

  router.route('/products/:id').put(productController.updateProduct);
};