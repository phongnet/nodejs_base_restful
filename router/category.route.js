const categoryController = require('../controllers/category.controller');

module.exports = router => {
  router.route('/categories').get(categoryController.getAll);

  router.route('/categories').post(categoryController.create);

  router.route('/categories/:code').get(categoryController.getByCode);
};
