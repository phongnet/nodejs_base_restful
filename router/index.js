// parent routing
const categoryRoute = require('./category.route');
const productRoute = require('./product.route');

module.exports = router => {
  categoryRoute(router); // APIs for category module
  productRoute(router); // route for product
};
