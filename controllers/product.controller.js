const Product = require('../models/Product');
const Category = require('../models/Category');
var _ = require('underscore');

module.exports = {
    create: (req, res, next) => {
    const {
      name,
      price,
      description,
      imageUrl,
      categoryCode
    } = req.body;
    const createdDate = new Date();
    const code = generateCode(categoryCode);

    saveProduct(categoryCode, {
      code,
      name,
      price,
      description,
      createdDate,
    });

    function saveProduct(categoryCode, obj) {
      Category.findOne({ code: categoryCode }).exec((err, _category) => {
        console.log(_category);
        if (err) res.send(err);
        else if (!_category) res.sendStatus(400);
        else {
          obj.category = _category._id;
          new Product(obj).save((err, _product) => {
            if (err) res.send(err);
            else if (!_category) res.sendStatus(400);
            else res.status(200).send(_product);
          });
        }
      });
    }

    function generateCode (_categoryCode) {
      const _code = new Date().getTime();
      switch (_categoryCode) {
        case 1: return ("RAU-" + _code);
        case 2: return ("CU-" + _code);
        case 3: return ("QUA-" + _code);
        default: return ("KHAC-" + _code);
      }
    }
  },
  getAll: (req, res, next) => {
    Product.find()
      .exec((err, items) => {
        if (err) res.send(err);
        else res.send(items);
      });
  },
  getById: (req, res, next) => {
    const _id = req.params.id;
    Product.findById(_id).exec((err, item, next) => {
      if (err) res.send(err);
      else if (!item) res.sendStatus(404);
      else res.send(item);
    });
  },
  getByCode: (req, res, next) => {
    const _code = req.query.code;
    Product.findOne({code: _code}).exec((err, item, next) => {
      if (err) res.send(err);
      else if (!item) res.sendStatus(404);
      else res.send(item);
    });
  },
  getByProviderId: (req, res, next) => {
    const _providerId = req.query.providerId;
    Product.findOne({provider: _providerId}).exec((err, item, next) => {
      if (err) res.send(err);
      else if (!item) res.sendStatus(404);
      else res.send(item);
    });
  },
  deleteProduct: (req, res) => {
    const _id = req.params.id;
    Product.findByIdAndRemove(_id).exec((err, item) => {
      if (err) res.send(err);
      else if (!item) res.sendStatus(404);
      else res.send({message: "Note deleted successfully!"});
    });
  }, 
  updateProduct: (req, res) => {
    const _id = req.params.id;
    const {
      name,
      price,
      description
    } = req.body;
    Product.findByIdAndUpdate(_id).exec((err, item) => {
      if (err) res.send(err);
      else if (!item) res.sendStatus(404);
      else res.send({message: "Update product successfully!"});
    });
  }
}