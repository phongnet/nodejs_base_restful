const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema(
  {
    code: String,
    name: String,
    price: Number,
    description: String,
    imageUrl: String,
    createdDate: Date,
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Category'
    }
  }
);
module.exports = mongoose.model('Product', ProductSchema);