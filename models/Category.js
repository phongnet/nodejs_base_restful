// server/models/User.js
const mongoose = require('mongoose');

const CategorySchema = new mongoose.Schema(
    {
        code: Number,
        name: String,
        description: String,
        imageUrl: String
    }
);
module.exports = mongoose.model('Category', CategorySchema);